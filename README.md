# trell-templates v1.0.0

Trell Docker-Django

## Cookiecutter templates + trellip

These templates make use of [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/index.html) templates and in conjunction with [trellip](https://bitbucket.org/trelltech/trell-ansible-roles/src/trellip/) should make bootstrapping a project as quick and simple as possible and help keep projects' structures uniform (assuming we don't end up with too many different templates).

Feel free to make changes, additions and/or improvements, but keep in mind that a change in one branch should perhaps be made in others for consistency, and updating READMEs should not be neglected. Also, if creating a new branch, remember to update the README to reflect the new branch.

### Project setup:

See [trellip](https://bitbucket.org/trelltech/trell-ansible-roles/src/trellip-cookiecutter/) docs.

### Build context via trellip

If electing to build via trellip, the `cookiecuter.json` and `repo_vars.json` files should be defined as follows:

```
{
    "< variable name >": {
        "form_type": "< form_type key (e.g. "inputbox") >",
        "form_config": {
            "text": "< Text to present to user >",
            // choices for formtype "menu"
            "choices": [< choices list of tuples (e.g. ("1", "True")) >]
        },
        // initial value for formtype inputbox
        "initial": "< initial value >",
        // optional - will override initial
        "initial_handler": "< initial_handler key (e.g. "project_name") >",
        // optional
        "validators": [< validator keys list (e.g. "present", "python") >]
    },
    ...
}
```
Form_type keys supported in trellip (others can be added if we see a need for them):

    "inputbox"
    "menu"

Initial_handler keys supported in trellip:

    "project_name": Uses project_name supplied in trellip
    "project_server_staging": Will try to determine server address/hostname based on project_name supplied in trellip, or None
    "project_server_production": Will try to determine server address/hostname based on project_name supplied in trellip, or None
    "random12": Will generate a random 12 char string of letters/numbers
    "random50": Will generate a random 50 char string of letters/numbers

Validator keys currently available in trellip:

    "repo": Validates valid repo address (only git atm)
    "directory": Validates valid directory name (only a-z, numbers, hyphens and underscore)
    "py_module": Same as directory, but disallows hyphens and must begin with letter
    "dir_no_underscore": Same as directory, but disallows underscores and must begin with letter
    "presence": Validates not empty
    "local_path": Validates path exists

***Important***
Repository variables defined in `repo_vars.json` should use `<variable name>` keys that are all UPPERCASE, and are prefixed with one of

    STAGING (e.g. "STAGING_HOST")
    PRODUCTION
    GLOBAL

Otherwise, if electing not to include support for trellip in your template, a `cookiecutter.json` file with simple key/value pairs will suffice.

After your project has been bootstrapped, `git init` and commit and push to provider you specified in trellip.

## Project specific

### Config

This template uses django-split-configs which can load configs sequentially as well as optionally, depending on the env and if a specified file exists. See `trellcc/settings/__init__.py`.

### Development

Spin up your development environment with `docker-compose up`

### LetsEncrypt

This template uses a certbot container which obtains certs for a requested domain via letsencrypt. This is on by default, but can be disabled in staging by adding a deployment variable (see below).

In order to set up LetsEncrypt, after the initial deployment you should notice that nginx will fail to start.

You may need to edit the first line of the relevant script, as well as the `docker/{ env }/nginx/nginx.conf`, if your project name does not match the domain name which will be registered (these use a www.{ project_name }.com template). After checking/making changes, commit your changes and then ssh into the host machine and navigate to the `/opt/{ project_name }/letsencrypt` directory. Finally, depending on the environmnet you're setting up, run:
```
chmod +x init-letsencrypt-{environment}.sh
sudo ./init-letsencrypt-{environment}.sh
```
This should generate certs - re-deploy and confirm you can reach your server via SSL/TLS.

### Deployment via BitBucket


Deployed via BitBucket's pipelines, after setting up via trellip.

Before deploying via BitBucket you'll need to add the host machine's address to known_hosts in the repo (Settings > Pipelines > SSH Keys). If not using trellip, SSH keys will need to be configured between the host and provider manually.

Finally, the following Pipelines Repository Variables should be configured. *Repo variables were chosen over deployment specific variables as they can be set via API*

***TO DO***

Add upload of known_host to provider via trellip.


```
$STAGING_USER (user on host)
$STAGING_HOST (host ip or domain)
$STAGING_DB_PWD
$STAGING_DB_USER
$STAGING_DB_NAME
$STAGING_SSL_DISABLED ('true' or 'false')
$STAGING_SECRET_KEY

$PRODUCTION_USER (user on host)
$PRODUCTION_HOST (host ip or domain)
$PRODUCTION_DB_PWD
$PRODUCTION_DB_USER
$PRODUCTION_DB_NAME
$PRODUCTION__SSL_DISABLED ('true' or 'false')
$PRODUCTION__SECRET_KEY
```

Deployments to staging will be triggered by successful merges, while production will require a manual trigger.

### To do

- Update test_skeleton
- Test with ssl with domain name. Have used these containers before and performed the steps described above, but have not tested after build from this repo. If anyone successfully sets up ssl please remove this.via API*
