#!/usr/bin/env bash

cd /app/{{cookiecutter.project_name}}

function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$DB_NAME", user="$DB_USER", password="$DB_PWD", host="db")
    conn.close()
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

python manage.py migrate \
&& python manage.py collectstatic --noinput -v0 \
&& python manage.py compress

echo Starting server...
gunicorn -b :9000 {{cookiecutter.main_app_name}}.wsgi
