import os

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "assets"),
)

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

COMPRESS_OFFLINE = False

COMPRESS_OUTPUT_DIR = ''

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', f'{NODE_MODULES_DIR}/.bin/node-sass {{ "{{" }}infile{{ "}}" }} {{ "{{" }}outfile{{ "}}" }}'),
    # Only needed if you are using React and/or ES6. Note the user of
    # browserifyinc here
    ('text/jsx', f'NODE_ENV=development {NODE_MODULES_DIR}/.bin/browserifyinc --debug -t babelify {{ "{{" }}infile{{ "}}" }} -o {{ "{{" }}outfile{{ "}}" }}'),
)

if not DEBUG:
    COMPRESS_OFFLINE = True
    COMPRESS_PRECOMPILERS = (
        ('text/x-scss', f'{NODE_MODULES_DIR}/.bin/node-sass {{ "{{" }}infile{{ "}}" }} {{ "{{" }}outfile{{ "}}" }}'),
        # Only needed if you are using React and/or ES6 ###
        ('text/jsx', f'NODE_ENV=production {NODE_MODULES_DIR}/.bin/browserify --debug -t babelify {{ "{{" }}infile{{ "}}" }} -o {{ "{{" }}outfile{{ "}}" }}')
    )
