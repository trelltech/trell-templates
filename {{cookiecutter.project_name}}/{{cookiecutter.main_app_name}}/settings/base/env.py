from os import environ

DEBUG = True
ENV = environ.get('DJANGO_ENV') or 'development'

if ENV in ['production', 'staging']:
    DEBUG = False
