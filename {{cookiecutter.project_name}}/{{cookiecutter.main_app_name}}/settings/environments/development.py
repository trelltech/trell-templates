from os import environ as _


ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': _['DB_NAME'],
        'USER': _['DB_USER'],
        'PASSWORD': _['DB_PWD'],
        'PORT': '5432',
        'HOST': 'db',
        'CONN_MAX_AGE': 900
    }
}
