"""
This is a django-split-settings main file.
For more information read this:
https://github.com/sobolevn/django-split-settings
Default environment is `developement`.
To change settings file:
`DJANGO_ENV=production python manage.py runserver`
"""

from split_settings.tools import optional, include
from os import environ

ENV = environ.get('DJANGO_ENV') or 'development'

include(
    # Load environment settings
    'base/env.py',

    # Here we should have the order because of dependencies
    'base/paths.py',
    'base/apps.py',
    'base/middleware.py',
    'base/debug_toolbar.py',

    # Load all other settings
    'base/*.py',

    # Select the right env:
    optional('environments/%s.py' % ENV),
)
