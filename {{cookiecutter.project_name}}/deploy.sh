#!/bin/bash

echo "Connected to server!"

cd $1

if [ ! -d $1/$2 ]
then
    echo "First run. Cloning "$3" into "$1" and checking out "$4"."
    git clone $3 && cd $1/$2 \
    && git checkout . && git checkout staging && git pull
else
    echo "Pulling latest changes to "$4" branch."
    cd $1/$2
    git checkout .
    git checkout staging
    git pull
fi

echo "Prerequisites installed, time to spin up the docker containers..."
export DB_PWD=$5
export DB_USER=$6
export DB_NAME=$7
export SERVER_IP=$8
export SECRET_KEY=$9

if [[ ${10} == 'staging' && ${11} == 'true' ]]
then
    compose_file='docker/'${10}'/docker-compose.no-ssl.yml'
else
    compose_file='docker/'${10}'/docker-compose.yml'
fi

sudo -E docker-compose -f $compose_file stop
sudo -E docker-compose -f $compose_file up --build -d

echo "Exiting..."
exit
